from typing import List

from github import Github
from github.Repository import Repository
from github.Commit import Commit


class SearchService:

    def __init__(self, github_client: Github):
        self._github_client = github_client

    def search_repositories(self, query, limit):
        repositories = self._github_client.search_repositories(
            query=query,
            **{'in': 'name'},
        )
        return [
            self._format_repo(repository)
            for repository in repositories[:limit]
        ]

    def _format_repo(self, repository: Repository):
        return {
            'url': repository.html_url,
            'name': repository.name,
            'owner': self._format_owner(repository.owner),
            'latest_commit': self._format_latest_commit(repository.get_commits()),
        }

    def _format_owner(self, owner):
         return {
            'login': owner.login,
            'url': owner.html_url,
            'avatar_url': owner.avatar_url,
        }

    def _format_latest_commit(self, commits: List[Commit]):
        if commits:
            return self._format_commit(commits[0])
        return {}

    def _format_commit(self, commit: Commit):
        return {
            'sha': commit.sha,
            'url': commit.html_url,
            'message': commit.commit.message,
            'author_name': commit.commit.author.name,
        }
